# Arch Hardening Script

A script that hardens Arch Linux for privacy and security a lot.

This script mostly automates [my guide](https://theprivacyguide1.github.io/linux_hardening_guide.html) on hardening Arch Linux.

A list of what it does:

* Kernel hardening via sysctl and boot parameters
* Disables IPv6 to reduce attack surface
* Mounts /proc with hidepid=2 to hide other users' processes
* Disables the potentially dangerous Netfilter automatic conntrack helper assignment to reduce attack surface
* Installs linux-hardened
* Enables AppArmor
* Installs Firejail
* Restricts root access
* Installs and configures UFW as a firewall
* Sets up Tor
* Changes your hostname to a generic one such as host
* Blocks all wireless devices with rfkill and blacklists the bluetooth kernel modules
* Creates a systemd service to spoof your MAC address at boot
* Uses a more restrictive umask
* Installs usbguard to blacklist USB devices
* Blacklists Thunderbolt and Firewire to prevent some DMA attacks
* Disables coredumps
* Enables microcode updates
* Disables NTP
* Enables IPv6 privacy extensions if IPv6 has not been disabled
* Blacklists uncommon network protocols
* Blacklists uncommon filesystems
* Installs haveged and jitterentropy to gather more entropy
* Blacklists the webcam, microphone and speaker kernel modules to prevent them from being used to spy on you

All of these are completely optional and you will be asked if you want them or not.

This script only works on Arch Linux or Manjaro with GRUB as the bootloader and systemd as the init system. If you want to disable any checks then run the script with the `--disable-checks` flag.

## How to use it:

Download the script and run `sh hardening.sh` or `chmod +x hardening.sh && ./hardening.sh`
